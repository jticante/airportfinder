//
//  Extensions.swift
//  AirportData
//
//  Created by JUAN T on 08/04/21.
//

import Foundation
import UIKit

extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    class var backgroundMainColor : UIColor {
        return UIColor.rgb(red: 235, green: 235, blue: 235)
    }
    
    class var backgroundButtonAction : UIColor {
        return UIColor.rgb(red: 65, green: 170, blue: 215)
    }
    
    class var backgroundLabelBigNumber: UIColor {
        return UIColor.rgb(red: 145, green: 145, blue: 145)
    }
    
    class var backgroundSliderActionRadiusKM: UIColor {
        return UIColor.rgb(red: 133, green: 209, blue: 241)
    }
    
    class var textColorInfo: UIColor {
        return UIColor.rgb(red: 196, green: 196, blue: 196)
    }
}

var bgSpinnerView : UIView?

extension UIViewController {
    func showSpinner(onView : UIView, bgColor : UIColor, indicatorColor : UIColor) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = bgColor
        let indicator = UIActivityIndicatorView.init(style: .gray)
        indicator.color = indicatorColor
        indicator.startAnimating()
        indicator.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(indicator)
            onView.addSubview(spinnerView)
        }
        bgSpinnerView = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            bgSpinnerView?.removeFromSuperview()
            bgSpinnerView = nil
        }
    }
}

extension String {
    func hexStringToUIColor () -> UIColor {
        var cString:String = self.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
