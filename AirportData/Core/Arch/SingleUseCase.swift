//
//  SingleUseCase.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation
import RxSwift

class SingleUseCase<Params, T> : GenericObservableUseCase<Params, Single<T>> {
    
    func executeAndReturnDisposable(params : Params?, observer : SingleObserver<T>) -> Disposable? {
        let observable = buildUseCase(params: params)
            .subscribe(on: ConcurrentDispatchQueueScheduler(qos: .background))
            .observe(on: MainScheduler.instance)
        return subscribeAndReturnDisposable(subscription:
                                                observable.subscribe(
                                                    onSuccess: { element in
                                                        observer.onSuccess(value: element)
                                                    }, onFailure: { error in
                observer.onError(error: error)
            }))
    }
}
