//
//  GenericObservableUseCase.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation
import RxSwift

class GenericObservableUseCase<Params, Observable : Any>{
    
    private var disposable  : Disposable?
    
    
    func buildUseCase(params : Params? = nil) -> Observable {
        preconditionFailure("This method should be implemented")
    }
    
    func subscribe(subscription : Disposable? = nil) {
        self.disposable = subscription
    }
    
    func subscribeAndReturnDisposable(subscription : Disposable? = nil) -> Disposable? {
        self.disposable = subscription
        return self.disposable
    }
    
    func dispose() {
        self.disposable?.dispose()
    }
}
