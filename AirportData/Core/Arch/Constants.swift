//
//  Constants.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation

struct Constants {
    static let BASE_URL = "https://aerodatabox.p.rapidapi.com/airports/"
    static let API_KEY = "6577d2048amsh7b5574371172805p1e63b1jsn42d008f2e694"
    static let HOST = "aerodatabox.p.rapidapi.com"
    static let MESSAJE_FOR_EMPTY = "Sorry but no results were found."
}
