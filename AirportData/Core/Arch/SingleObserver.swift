//
//  SingleObserver.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation

class SingleObserver<T> {
    func onSuccess(value : T) {
        preconditionFailure("This method should be implemented")
    }
    
    func onError(error : Error){
        preconditionFailure("This method should be implemented")
    }
}
