//
//  AppError.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation

public enum AppError : Error {
    case NullParametersError
}
