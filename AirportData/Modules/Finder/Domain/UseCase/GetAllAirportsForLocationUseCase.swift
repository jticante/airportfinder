//
//  GetAllAirportsForLocationUseCase.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation
import RxSwift

class GetAllAirportsForLocationUseCase: SingleUseCase<GetAllAirportsForLocationUseCase.Params, [ItemsEntity]> {
    
    private var airportsSourceRepository : AirportsSourceRepository!
    
     init(airportsSourceRepository : AirportsSourceRepository) {
        self.airportsSourceRepository = airportsSourceRepository
     }
    
    override func buildUseCase(params: GetAllAirportsForLocationUseCase.Params?) -> Single<[ItemsEntity]> {
        if let paramsRequest = params{
            return airportsSourceRepository.retrieveAllAirportsForLocation(myLat: paramsRequest.myLat, myLon: paramsRequest.myLon, km: paramsRequest.km)
        } else{
            return Single.error(AppError.NullParametersError)
        }
    }
    
    public struct Params {
        let myLat : String
        let myLon : String
        let km : String
        
        init(myLat : String,  myLon : String, km : String) {
            self.myLat = myLat
            self.myLon = myLon
            self.km = km
        }
    }
}
