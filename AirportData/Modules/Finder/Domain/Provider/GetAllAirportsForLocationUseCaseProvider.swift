//
//  GetAllAirportsForLocationUseCaseProvider.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation

protocol GetAllAirportsForLocationUseCaseProvider {
    func providegetAllAirportsForLocationUseCase() -> GetAllAirportsForLocationUseCase
}
