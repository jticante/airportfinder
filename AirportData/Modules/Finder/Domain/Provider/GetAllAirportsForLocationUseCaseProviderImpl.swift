//
//  GetAllAirportsForLocationUseCaseProviderImpl.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation

class GetAllAirportsForLocationUseCaseProviderImpl : GetAllAirportsForLocationUseCaseProvider {
    private var airportsSourceRepository : AirportsSourceRepository!
    
    init(airportsSourceRepository : AirportsSourceRepository) {
       self.airportsSourceRepository = airportsSourceRepository
    }
    
    func providegetAllAirportsForLocationUseCase() -> GetAllAirportsForLocationUseCase {
        return GetAllAirportsForLocationUseCase(airportsSourceRepository: airportsSourceRepository)
    }
    
}
