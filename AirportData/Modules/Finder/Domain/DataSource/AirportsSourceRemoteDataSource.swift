//
//  AirportsSourceRemoteDataSource.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation
import RxSwift

protocol AirportsSourceRemoteDataSource {
    func retrieveAllAirportsForLocation(myLat : String , myLon : String, km : String) -> Single<[ItemsEntity]>
}
