//
//  ItemsEntity.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation

public struct ItemsEntity{
    let icao : String?
    let iata : String?
    let name : String?
    let shortName : String?
    let municipalityName : String?
    let location : LocationEntity?
    let countryCode : String?
    
    public init(icao : String?,iata : String?,name : String?,shortName : String?,municipalityName : String?,location : LocationEntity?,countryCode : String?){
        self.icao = icao
        self.iata = iata
        self.name = name
        self.shortName = shortName
        self.municipalityName = municipalityName
        self.location = location
        self.countryCode = countryCode
    }
}
