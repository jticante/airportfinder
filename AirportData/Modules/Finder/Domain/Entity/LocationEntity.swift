//
//  LocationEntity.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation

public struct LocationEntity{
    let lat : Double?
    let lon : Double?
    
    public init(lat : Double?, lon : Double?){
        self.lat = lat
        self.lon = lon
    }
}
