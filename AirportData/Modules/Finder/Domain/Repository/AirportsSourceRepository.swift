//
//  AirportsSourceRepository.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation
import RxSwift

protocol AirportsSourceRepository {
    func retrieveAllAirportsForLocation(myLat : String , myLon : String, km : String) -> Single<[ItemsEntity]>
}
