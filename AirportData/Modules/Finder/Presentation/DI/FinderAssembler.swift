//
//  FinderAssembler.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation

protocol FinderAssembler {
    func resolve(finderSourceView : FinderSourceView) -> FinderPresenter
    func resolve() -> GetAllAirportsForLocationUseCaseProvider
    func resolve() -> AirportsSourceRepository
    func resolve() -> AirportsSourceRemoteDataSource
    func resolve() -> ItemsToItemsEntity
    func resolve() -> LocationToLocationEntity
}

extension FinderAssembler {
    
    func resolve(finderSourceView : FinderSourceView) -> FinderPresenter {
        return FinderPresenterImpl(getAllAirportsForLocationUseCaseProvider: resolve(), finderSourceView: finderSourceView)
    }
    
    func resolve() -> GetAllAirportsForLocationUseCaseProvider {
        return GetAllAirportsForLocationUseCaseProviderImpl(airportsSourceRepository: resolve())
    }
    
    func resolve() -> AirportsSourceRepository {
        return AirportsSourceRepositoryImpl(airportsSourceRemoteDataSource: resolve())
    }
    
    func resolve() -> AirportsSourceRemoteDataSource {
        return AirportsSourceRemoteDataSourceImpl(itemsToItemsEntity: resolve())
    }
    
    func resolve() -> ItemsToItemsEntity {
        return ItemsToItemsEntity(locationToLocationEntity: resolve())
    }
    
    func resolve() -> LocationToLocationEntity {
        return LocationToLocationEntity()
    }
}

class FinderAssemblerInjector : FinderAssembler {
    
}
