//
//  FinderViewController.swift
//  AirportData
//
//  Created by JUAN T on 08/04/21.
//

import UIKit
import CoreLocation
import Toast_Swift

class FinderViewController: UIViewController, FinderSourceView {
    
    var safeArea : UILayoutGuide!
    var btnSearch : UIButton!
    var lblTitleApp : UILabel!
    var lblSubtitleApp : UILabel!
    var lblRadiusKM : UILabel!
    var sliderRadiusKM : UISlider!
    var lblInfoRadiusKM : UILabel!
    
    private var finderPresenter : FinderPresenter!
    private let npsAssemblerInjector : FinderAssemblerInjector = FinderAssemblerInjector()
    private var router = FinderRouter()
    
    private var locationManager = CLLocationManager()
    var updatedUserLocation :CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewsOnFinder()
        self.finderPresenter = self.npsAssemblerInjector.resolve(finderSourceView: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getDeviceLocation()
    }
    
    func getDeviceLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    func setupViewsOnFinder() {
        safeArea = view.safeAreaLayoutGuide
        self.view.backgroundColor = .backgroundMainColor
        
        lblTitleApp = makeLabelTitle(withText: "AIRPORT")
        view.addSubview(lblTitleApp)
        lblTitleApp.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lblTitleApp.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 32).isActive = true
        
        lblSubtitleApp = makeLabelSubTitle(withText: "finder")
        view.addSubview(lblSubtitleApp)
        lblSubtitleApp.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lblSubtitleApp.topAnchor.constraint(equalTo: lblTitleApp.bottomAnchor, constant: 0).isActive = true
        
        lblRadiusKM = makeLabelBigNumber(withText: "60")
        view.addSubview(lblRadiusKM)
        lblRadiusKM.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lblRadiusKM.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        
        sliderRadiusKM = makeSliderActionRadiusKM()
        sliderRadiusKM.value = Float(60)
        view.addSubview(sliderRadiusKM)
        sliderRadiusKM.topAnchor.constraint(equalTo: lblRadiusKM.bottomAnchor, constant: 0).isActive = true
        sliderRadiusKM.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 32).isActive = true
        sliderRadiusKM.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -32).isActive = true
        sliderRadiusKM.addTarget(self, action: #selector(FinderViewController.sliderValueChanged) ,for:  UIControl.Event.allEvents)
        
        lblInfoRadiusKM = makeLabelInfo(withText: "RADIUS IN KM")
        view.addSubview(lblInfoRadiusKM)
        lblInfoRadiusKM.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        lblInfoRadiusKM.topAnchor.constraint(equalTo: sliderRadiusKM.bottomAnchor, constant: 8).isActive = true
        
        
        btnSearch = makeButtonAction(withText: "SEARCH")
        view.addSubview(btnSearch)
        btnSearch.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        btnSearch.widthAnchor.constraint(equalToConstant: 300).isActive = true
        btnSearch.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btnSearch.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: -20).isActive = true
        btnSearch.addTarget(self,action: #selector(buttonActionSearch),for: .touchUpInside)
    }
    
    
    func makeLabelTitle(withText text:String) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.text = text
        label.textAlignment = .justified
        label.font = UIFont(name: "GothamRounded-Book", size: 48)
        return label
    }
    
    func makeLabelSubTitle(withText text:String) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.text = text
        label.font = UIFont(name: "GothamRounded-Medium", size: 24)
        return label
    }
    
    func makeLabelBigNumber(withText text:String) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .backgroundLabelBigNumber
        label.text = text
        label.font = UIFont(name: "GothamRounded-Bold", size: 80)
        return label
    }
    
    func makeSliderActionRadiusKM() -> UISlider {
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.minimumValue = 1
        slider.maximumValue = 1000
        slider.isContinuous = true
        slider.tintColor = .backgroundSliderActionRadiusKM
        return slider
    }
    
    func makeLabelInfo(withText text:String) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .textColorInfo
        label.text = text
        label.font = UIFont(name: "GothamRounded-Medium", size: 20)
        return label
    }
    
    
    func makeButtonAction(withText text:String) -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(text, for: UIControl.State.normal)
        button.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: UIControl.State.normal)
        button.backgroundColor = .backgroundButtonAction
        button.layer.cornerRadius = 15.0
        button.titleLabel?.font = UIFont(name: "GothamRounded-Bold", size: 16)
        return button
    }
    
    @objc
    func buttonActionSearch() {
        if let userLocation = updatedUserLocation{
            finderPresenter.retrieveAllAirportsForLocation(myLat: "\(userLocation.coordinate.latitude)", myLon: "\( userLocation.coordinate.longitude)", km: lblRadiusKM.text ?? "1")
        }
    }
    
    @objc func sliderValueChanged(sender: UISlider) {
        let value = Int(sliderRadiusKM.value)
        lblRadiusKM.text = "\(value)"
    }
    
    func showError(message: String) {
        self.view.makeToast(message)
    }
    
    func showLoader() {
        self.showSpinner(onView: self.view, bgColor: .white, indicatorColor: "#1b75bb".hexStringToUIColor())
    }
    
    func hideLoader() {
        self.removeSpinner()
    }
    
    func diplayAirportsForLocation(allAirports: [ItemsEntity]) {
        if allAirports.isEmpty{
            self.view.makeToast(Constants.MESSAJE_FOR_EMPTY)
        } else{
            finderPresenter.goToResultsViews(view: self, router: router, airports: allAirports, updatedUserLocation: updatedUserLocation)
        }
    }
}

extension FinderViewController : CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch(CLLocationManager.authorizationStatus()) {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        case .denied, .notDetermined, .restricted:
            locationManager.stopUpdatingLocation()
        @unknown default:
            fatalError()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation = locations.last
        updatedUserLocation = userLocation
        self.locationManager.stopUpdatingLocation()
        self.locationManager.delegate = nil
    }
}
