//
//  FinderSourceView.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation

protocol FinderSourceView {
    func showError(message : String)
    func showLoader()
    func hideLoader()
    func diplayAirportsForLocation(allAirports : [ItemsEntity])
}
