//
//  FinderPresenterImpl.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation
import RxSwift
import CoreLocation

class FinderPresenterImpl: FinderPresenter {
    private var getAllAirportsForLocationUseCaseProvider : GetAllAirportsForLocationUseCaseProvider!
    private var finderSourceView : FinderSourceView!
    private var disposeBag : DisposeBag = DisposeBag()
    
    init(getAllAirportsForLocationUseCaseProvider : GetAllAirportsForLocationUseCaseProvider,finderSourceView : FinderSourceView) {
        self.getAllAirportsForLocationUseCaseProvider = getAllAirportsForLocationUseCaseProvider
        self.finderSourceView = finderSourceView
    }
    
    func retrieveAllAirportsForLocation(myLat: String, myLon: String, km: String) {
        self.finderSourceView.showLoader()
        self.getAllAirportsForLocationUseCaseProvider.providegetAllAirportsForLocationUseCase().executeAndReturnDisposable(params: GetAllAirportsForLocationUseCase.Params(myLat: myLat, myLon: myLon, km: km), observer: GetAirportsForLocationObserver(view: finderSourceView))?.disposed(by: disposeBag)
    }
    
    class GetAirportsForLocationObserver : SingleObserver<[ItemsEntity]> {
        
        private var view : FinderSourceView!
        
        init(view : FinderSourceView) {
            self.view = view
        }
        
        override func onSuccess(value: [ItemsEntity]) {
            self.view.hideLoader()
            self.view.diplayAirportsForLocation(allAirports: value)
        }
        
        override func onError(error: Error) {
            self.view.hideLoader()
            self.view.showError(message: "Ha ocurrido un error")
        }
    }

    func goToResultsViews(view: FinderViewController, router: FinderRouter, airports: [ItemsEntity], updatedUserLocation: CLLocation?) {
        router.setSourceView(view)
        router.createTabBarLoadDataController(airports: airports, updatedUserLocation: updatedUserLocation)
    }
    
}
