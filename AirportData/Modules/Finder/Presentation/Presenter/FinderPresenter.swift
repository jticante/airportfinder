//
//  FinderPresenter.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation
import CoreLocation

protocol FinderPresenter {
    func retrieveAllAirportsForLocation(myLat : String , myLon : String, km : String)
    func goToResultsViews(view : FinderViewController,router : FinderRouter,airports: [ItemsEntity],updatedUserLocation :CLLocation?)
}
