//
//  LocationToLocationEntity.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation

class LocationToLocationEntity : Transformer<Location, LocationEntity> {
    
    override func transform(value: Location) -> LocationEntity {
        return LocationEntity(lat: value.lat, lon: value.lon)
    }
    
}
