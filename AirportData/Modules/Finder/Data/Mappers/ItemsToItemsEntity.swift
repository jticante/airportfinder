//
//  ItemsToItemsEntity.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation

class ItemsToItemsEntity : Transformer<Items, ItemsEntity> {
    
    private var locationToLocationEntity : LocationToLocationEntity!
    
    init(locationToLocationEntity : LocationToLocationEntity) {
        self.locationToLocationEntity = locationToLocationEntity
    }
    
    override func transform(value: Items) -> ItemsEntity {
        var locationToTransform : Location!
        if let locationValue = value.location{
            locationToTransform = locationValue
        }
        return ItemsEntity(icao: value.icao, iata: value.iata, name: value.name, shortName: value.shortName, municipalityName: value.municipalityName, location: locationToLocationEntity.transform(value: locationToTransform), countryCode: value.countryCode)
    }
    
}

