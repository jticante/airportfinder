//
//  AirportsSourceRepositoryImpl.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation
import RxSwift

class AirportsSourceRepositoryImpl: AirportsSourceRepository {
    
    private var airportsSourceRemoteDataSource : AirportsSourceRemoteDataSource!
    
    init(airportsSourceRemoteDataSource : AirportsSourceRemoteDataSource ) {
        self.airportsSourceRemoteDataSource = airportsSourceRemoteDataSource
    }
    
    func retrieveAllAirportsForLocation(myLat : String , myLon : String, km : String) -> Single<[ItemsEntity]> {
        return airportsSourceRemoteDataSource.retrieveAllAirportsForLocation(myLat: myLat, myLon: myLon, km: km)
    }

}
