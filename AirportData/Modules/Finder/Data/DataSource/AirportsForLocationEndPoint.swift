//
//  AirportsForLocationEndPoint.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation

struct AirportsForLocationEndPoint {
    
    static func getAirportsForLocation(myLat : String , myLon : String, km : String) -> String {
        return "\(Constants.BASE_URL)search/location/\(myLat)/\(myLon)/km/\(km)/20"
    }
    
}
