//
//  AirportsSourceRemoteDataSourceImpl.swift
//  AirportData
//
//  Created by JUAN T on 10/04/21.
//

import Foundation
import RxSwift

class AirportsSourceRemoteDataSourceImpl: AirportsSourceRemoteDataSource {
    
    let headers = [
        "x-rapidapi-key": Constants.API_KEY,
        "x-rapidapi-host": Constants.HOST
    ]
    
    private var itemsToItemsEntity : ItemsToItemsEntity
    
    init(itemsToItemsEntity : ItemsToItemsEntity) {
        self.itemsToItemsEntity = itemsToItemsEntity
    }
    
    func retrieveAllAirportsForLocation(myLat : String , myLon : String, km : String) -> Single<[ItemsEntity]> {
        let observable = Single<[ItemsEntity]>.create { (single) -> Disposable in
            let urlString = AirportsForLocationEndPoint.getAirportsForLocation(myLat: myLat, myLon: myLon, km: km)
            if let url = URL(string: urlString) {
                
                var request = URLRequest(url: url)
                request.httpMethod = "GET"
                request.allHTTPHeaderFields = self.headers
                request.addValue("application/json", forHTTPHeaderField: "Content-type")
                
                let urlSession = URLSession(configuration: .default).dataTask(with: request) { (data, response, error) in
                    if let error = error {
                        single(.failure(error))
                    }
                    if let data = data {
                        let result = try? JSONDecoder().decode(AirportsResponse.self, from: data)
                        single(.success(self.itemsToItemsEntity.transformCollection(values: result?.items ?? [Items]())))
                    }
                }
                urlSession.resume()
                return Disposables.create {
                    urlSession.cancel()
                }
            }
            return Disposables.create()
        }
        return observable
    }
    
}
