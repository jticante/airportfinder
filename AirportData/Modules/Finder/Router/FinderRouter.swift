//
//  FinderRouter.swift
//  AirportData
//
//  Created by JUAN T on 08/04/21.
//

import Foundation
import UIKit
import CoreLocation

class FinderRouter {
    private var sourceView : UIViewController?
    
    var viewController : UIViewController {
        return FinderViewController()
    }
    
    func setSourceView(_ sourceView : UIViewController?){
        guard let view =  sourceView  else { fatalError("UI Error")}
        self.sourceView = view
    }

    func createTabBarLoadDataController(airports: [ItemsEntity], updatedUserLocation: CLLocation?){
        let mapVC = MapRouter(airports: airports, updatedUserLocation: updatedUserLocation).viewController
        mapVC.navigationItem.title = "Map Mode"
        let mapaNC = UINavigationController(rootViewController: mapVC)
        mapaNC.tabBarItem.image = #imageLiteral(resourceName: "map")
    
        let listVC = ListRouter(airports: airports).viewController
        listVC.navigationItem.title = "List Mode"
        let listNC = UINavigationController(rootViewController: listVC)
        listNC.tabBarItem.image = #imageLiteral(resourceName: "list")
        
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [mapaNC,listNC]
        tabBarController.selectedIndex = 0
        self.sourceView?.present(tabBarController, animated: true, completion: nil)
    }
}
