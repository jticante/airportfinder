//
//  ListViewController.swift
//  AirportData
//
//  Created by JUAN T on 08/04/21.
//

import UIKit

class ListViewController: UIViewController {
    
    private var safeArea : UILayoutGuide!
    private let tableViewAirports = UITableView()
    var airports : [ItemsEntity]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        safeArea = view.safeAreaLayoutGuide
        self.view.backgroundColor = .backgroundMainColor
        tableViewAirports.dataSource = self
        tableViewAirports.delegate = self
        tableViewAirports.register(UINib(nibName: "AirportTableViewCell", bundle: nil), forCellReuseIdentifier: "airportTableViewCell")
        tableViewAirports.separatorStyle = .none
        tableViewAirports.backgroundColor = .backgroundMainColor
        setupTableViewAirports()
    }
    
     func setupTableViewAirports(){
        tableViewAirports.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableViewAirports)
        tableViewAirports.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 8).isActive = true
        tableViewAirports.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor,constant: 8).isActive = true
        tableViewAirports.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: -8).isActive = true
        tableViewAirports.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor,constant: -8).isActive = true
    }
}

extension ListViewController : UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return airports?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let airportcell = tableView.dequeueReusableCell(withIdentifier: "airportTableViewCell", for: indexPath) as! AirportTableViewCell
        let airport = self.airports?[indexPath.row]
        airportcell.selectionStyle = .none
        airportcell.lblName.text = "Name : \(airport?.name ?? "Unnamed")"
        airportcell.lblCity.text = "City : \(airport?.municipalityName ?? "Unknown place")"
        return airportcell
    }
    
}
