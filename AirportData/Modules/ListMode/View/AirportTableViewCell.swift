//
//  AirportTableViewCell.swift
//  AirportData
//
//  Created by JUAN T on 11/04/21.
//

import UIKit

class AirportTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentView.backgroundColor = .backgroundMainColor
        contentView.layer.cornerRadius = 0
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.shadowOpacity = 0.15
        contentView.layer.shadowRadius = 2.0
        contentView.layer.shadowOffset = CGSize(width: 0, height: 0)
        contentView.layer.masksToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
