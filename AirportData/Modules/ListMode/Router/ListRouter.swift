//
//  ListRouter.swift
//  AirportData
//
//  Created by JUAN T on 11/04/21.
//

import Foundation
import UIKit

class ListRouter {
    
    var viewController : UIViewController {
        return createViewController()
    }
    
    private var sourceView : UIViewController?
    
    var airports : [ItemsEntity]?
    
    init(airports : [ItemsEntity]? = nil) {
        self.airports = airports
    }
    
    func setSourceView(_ sourceView : UIViewController?){
        guard let view =  sourceView  else { fatalError("UI Error")}
        self.sourceView = view
    }
    
    private func createViewController() -> UIViewController {
        let view = ListViewController()
        view.airports = self.airports
        return view
    }
    
}
