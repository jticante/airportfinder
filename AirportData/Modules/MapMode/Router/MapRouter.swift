//
//  MapRouter.swift
//  AirportData
//
//  Created by JUAN T on 11/04/21.
//

import Foundation
import UIKit
import CoreLocation

class MapRouter {
    
    var viewController : UIViewController {
        return createViewController()
    }
    
    private var sourceView : UIViewController?
    
    var airports : [ItemsEntity]?
    var updatedUserLocation: CLLocation?
    
    init(airports : [ItemsEntity]? = nil, updatedUserLocation: CLLocation? = nil) {
        self.airports = airports
        self.updatedUserLocation = updatedUserLocation
    }
    
    func setSourceView(_ sourceView : UIViewController?){
        guard let view =  sourceView  else { fatalError("UI Error")}
        self.sourceView = view
    }
    
    private func createViewController() -> UIViewController {
        let view = MapViewController()
        view.airports = self.airports
        view.updatedUserLocation = self.updatedUserLocation
        return view
    }
    
}
