//
//  MapViewController.swift
//  AirportData
//
//  Created by JUAN T on 08/04/21.
//

import UIKit
import GoogleMaps
import CoreLocation

class MapViewController: UIViewController {
    
    private var safeArea : UILayoutGuide!
    private var mapView: GMSMapView!
    var airports : [ItemsEntity]?
    var updatedUserLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        safeArea = view.safeAreaLayoutGuide
        self.view.backgroundColor = .backgroundMainColor
        if let userLocation = updatedUserLocation{
            let camera = GMSCameraPosition.camera(withLatitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude, zoom: 8.0)
            mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
            mapView.isMyLocationEnabled = true
            mapView.delegate = self
            mapView.settings.myLocationButton = true
            self.view.addSubview(mapView)
            mapView.translatesAutoresizingMaskIntoConstraints = false
            mapView.topAnchor.constraint(equalTo: safeArea.topAnchor).isActive = true
            mapView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor).isActive = true
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
            displayAirports(airports: airports ?? [ItemsEntity]())
        }
    }
}

extension MapViewController : GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        if let airportData = marker.userData as? ItemsEntity{
            let camera = GMSCameraPosition.camera(withLatitude: airportData.location?.lat ?? 0, longitude: airportData.location?.lon ?? 0, zoom: 14)
            mapView.animate(to: camera)
        }
    }
    
    func displayAirports(airports : [ItemsEntity]) {
        for airport in airports {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude:airport.location?.lat ?? 0, longitude:airport.location?.lon ?? 0)
            marker.title = airport.name
            marker.snippet = ""
            marker.icon = #imageLiteral(resourceName: "flags")
            marker.userData = airport
            marker.map = mapView
        }
    }
    
}
